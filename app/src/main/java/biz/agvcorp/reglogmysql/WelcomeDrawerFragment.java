package biz.agvcorp.reglogmysql;

import android.app.Fragment;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Shazzad on 1/17/2018.
 */

public class WelcomeDrawerFragment extends Fragment {

    View mView;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private View mProgressView;
    private View mProfileFormView;

    private ImageView mProfileImageView;
    private TextView mNameView;
    private TextView mEmailView;
    private TextView mPhoneView;
    private TextView mAddressView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.home_activity_drawer, container, false);

        pref = mView.getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        mProfileFormView = mView.findViewById(R.id.home_form);
        mProgressView = mView.findViewById(R.id.home_progress);

        return mView;
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mProfileImageView = view.findViewById(R.id.home_profile_image);
        mNameView = view.findViewById(R.id.home_name);
        mEmailView = view.findViewById(R.id.home_email);
        mPhoneView = view.findViewById(R.id.home_phone);
        mAddressView = view.findViewById(R.id.home_address);

        final String url = getString(R.string.host_url)+"/"+getString(R.string.profile_view)+"/"+getCurrentId().toString();
//        Toast.makeText(view.getContext(), url, Toast.LENGTH_LONG).show();

        RequestQueue home_queue = Volley.newRequestQueue(view.getContext());

        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
//                Toast.makeText(view.getContext(), response.toString(), Toast.LENGTH_LONG).show();
                /*IMAGE UPLOADING TO SERVER AND PERIPHERALS*/
                try {
                    if (!response.getString("image").equals("noimage")) {
                        RequestQueue requestQueue = Volley.newRequestQueue(view.getContext());
                        String imageUrl = getString(R.string.host) + "/public/uploads/profileImages/" + response.getString("image");
                        ImageRequest profileImageRequest = new ImageRequest(imageUrl, new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                mProfileImageView.setImageBitmap(response);
                            }
                        }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        });
                        requestQueue.add(profileImageRequest);
                    } else {
                        mProfileImageView.setImageResource(R.drawable.userpicture);
                    }

                    mNameView.setText( response.getString("first_name").toUpperCase()+" "+response.getString("last_name").toUpperCase() );  //NULL AITASE
                    mEmailView.setText(response.getString("email"));
                    mPhoneView.setText(response.getString("mobile"));
                    mAddressView.setText(response.getString("address"));

                } catch (JSONException e) {
                    e.printStackTrace();
                    Snackbar.make(mProgressView, "Invalid Response From Server", Snackbar.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar.make(mProgressView, "Request Failed", Snackbar.LENGTH_LONG).show();
            }
        });

        home_queue.add(postRequest);
    }

    private String getCurrentId(){
        return pref.getString("logginID", "");
    }
}
