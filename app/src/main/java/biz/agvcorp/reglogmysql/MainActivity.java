package biz.agvcorp.reglogmysql;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends AppCompatActivity {

    private Button mLogin;
    private Button mRegistration;
    private Button mPhoneNumberRegistration;

    private LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = findViewById(R.id.mainWindow);

        mLogin = findViewById(R.id.btn_login_lan);
        mRegistration = findViewById(R.id.btn_registration_lan);
        mPhoneNumberRegistration = findViewById(R.id.btn_phoneNumber_lan);

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(MainActivity.this, SignInActivity.class);
                startActivity(loginIntent);
                finish();
            }
        });

        mRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(regIntent);
                finish();
            }
        });

        mPhoneNumberRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent phoneIntent = new Intent(MainActivity.this, SplashActivity.class);
                startActivity(phoneIntent);
                finish();
            }
        });
    }
}
