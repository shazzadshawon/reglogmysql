package biz.agvcorp.reglogmysql;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.kbeanie.multipicker.api.CameraImagePicker;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Shazzad on 1/17/2018.
 */

public class ProfileDrawerFragment extends Fragment implements DatePickerDialog.OnDateSetListener{
    private AppCompatActivity mActivity;

    View mView;
    Map<String, String> profileData;

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    private View mProgressView;
    private View mProfileFormView;

    private ProgressBar profileImageLoader;

    private ImageView mProfilePicView;
    private EditText mFirstNameView;
    private EditText mLastNameView;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private EditText mNidView;
    private EditText mPhoneNumberView;
    private EditText mAddressView;
    private EditText mDateOfBirthView;
    private AppCompatButton mProfileButton;

    private ImagePicker imagePicker;
    private Intent galleryIntent;
    private int PICK_IMAGE_REQUEST = 1;
    private static final int STORAGE_PERMISSION_CODE = 123;
    private Uri filePath;
    private String fileType;
    private Bitmap bitmap;
    private TextView mProfilePictureTitle;

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProfileFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);}
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);}
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showVolleyProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mProfileFormView.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);}
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);}
            });
        } else {
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProfileFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showImageProgress(final boolean show) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
            profileImageLoader.setVisibility(show ? View.GONE : View.VISIBLE);
            profileImageLoader.animate().setDuration(shortAnimTime).alpha(show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {profileImageLoader.setVisibility(show ? View.GONE : View.VISIBLE);}
            });
            profileImageLoader.setVisibility(show ? View.VISIBLE : View.GONE);
            profileImageLoader.animate().setDuration(shortAnimTime).alpha(show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {profileImageLoader.setVisibility(show ? View.VISIBLE : View.GONE);}
            });
        } else {
            profileImageLoader.setVisibility(show ? View.VISIBLE : View.GONE);
            profileImageLoader.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.profile_activity_drawer, container, false);

        pref = mView.getContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        mProfileFormView = mView.findViewById(R.id.profile_form);
        mProgressView = mView.findViewById(R.id.profile_progress);
        mProfileButton = mView.findViewById(R.id.btn_saveProfile);

        profileImageLoader = mView.findViewById(R.id.profile_image_progress);
        mProfilePictureTitle = mView.findViewById(R.id.profilePicturetitle);
//        mProfileButton.setEnabled(false);

        if (ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {
                Toast.makeText(mView.getContext(), "To upload profile picture from your Phone, we need to access permission to your Storage. Please provide permission to go ahead!", Toast.LENGTH_LONG).show();
            }
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }

        return mView;
    }

    public String getStringImage(Bitmap bmp) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, baos);
        //bmp.recycle();
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        //baos.close();
        return encodedImage;
    }

    public Bitmap getImageString(String imageString){
        byte[] imageBytes = Base64.decode(imageString, Base64.DEFAULT);
        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        return decodedImage;
    }

    private void showImageChooser(){
        galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(galleryIntent, "Select Your Profile Picture"), PICK_IMAGE_REQUEST);

        /*imagePicker = new ImagePicker(this);
        imagePicker.shouldGenerateMetadata(true);
        imagePicker.shouldGenerateThumbnails(true);
        imagePicker.setImagePickerCallback(this);
        imagePicker.pickImage();*/
    }

    //handling the image chooser activity result
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

/*        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_DEVICE) {
                if (imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            }
        }*/

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null){
            if (galleryIntent == null){
                showImageChooser();
            }

            filePath = data.getData();
            fileType = getActivity().getContentResolver().getType(filePath);

            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                mProfilePicView.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity().getApplicationContext(), "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity().getApplicationContext(), "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showProgress(true);

        mProfilePicView = view.findViewById(R.id.profilePicture);
        mFirstNameView = view.findViewById(R.id.firstname);
        mLastNameView = view.findViewById(R.id.lastname);
        mEmailView = view.findViewById(R.id.email);
        mPasswordView = view.findViewById(R.id.password);
        mNidView = view.findViewById(R.id.nid);
        mPhoneNumberView = view.findViewById(R.id.phone);
        mAddressView = view.findViewById(R.id.address);
        mDateOfBirthView = view.findViewById(R.id.dob);

        mDateOfBirthView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newDateDialogueFragment = new SelectDateFragment();
                newDateDialogueFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        mProfilePicView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageChooser();
            }
        });



        final String url = getString(R.string.host_url)+"/"+getString(R.string.profile_view)+"/"+getCurrentId().toString();
        Log.d("Current Url", url);

        RequestQueue queue = Volley.newRequestQueue(view.getContext());
        JsonObjectRequest postRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                showProgress(false);
                Log.d("ProfileResponse", response.toString());
//                mFirstNameView.setText("This is a Test Test test");
                try {
                    profileData = new HashMap<String, String>();
                    profileData.put("id",response.getString("id"));
                    profileData.put("image",response.getString("image"));
                    profileData.put("firstname",response.getString("first_name"));
                    profileData.put("lastname", response.getString("last_name"));
                    profileData.put("email",response.getString("email"));
                    profileData.put("password", response.getString("password"));
                    profileData.put("nid",response.getString("nid"));
                    profileData.put("phone", response.getString("mobile"));
                    profileData.put("dob", response.getString("dob"));
                    profileData.put("address", response.getString("address"));
                    profileData.put("imagesize", response.getString("imagesize"));

//                    if ( response.getString("image") != "noimage" ){
//                        Bitmap profileImage = getImageString(response.getString("image"));
//                        if (profileImage != null){
//                            mProfilePicView.setImageBitmap(profileImage);
//                        } else {
//                            mProfilePicView.setImageResource(R.drawable.userpicture);
//                        }
//                    }


                    /*IMAGE UPLOADING TO SERVER AND PERIPHERALS*/
                    if ( !profileData.get("image").equals("noimage")){
                        showImageProgress(true);
                        mProfilePicView.setVisibility(View.INVISIBLE);
                        mProfilePictureTitle.setVisibility(View.VISIBLE);
                        mProfilePictureTitle.setText("Loadin Image ...");
                        RequestQueue requestQueue = Volley.newRequestQueue(view.getContext());
                        String imageUrl = getString(R.string.host)+"/public/uploads/profileImages/"+response.getString("image");
                        ImageRequest profileImageRequest = new ImageRequest(imageUrl, new Response.Listener<Bitmap>() {
                            @Override
                            public void onResponse(Bitmap response) {
                                showImageProgress(false);
                                mProfilePicView.setVisibility(View.VISIBLE);
                                mProfilePicView.setImageBitmap(response);
                                mProfilePictureTitle.setVisibility(View.VISIBLE);
                                mProfilePictureTitle.setText("Image Size: "+profileData.get("imagesize")+" KB");
                            }
                        }, 0, 0, ImageView.ScaleType.CENTER_CROP, Bitmap.Config.RGB_565, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                mProfilePicView.setVisibility(View.VISIBLE);
                                mProfilePictureTitle.setVisibility(View.VISIBLE);
                                mProfilePictureTitle.setText("Image Loading Failed");
                                showImageProgress(false);
                            }
                        });
                        requestQueue.add(profileImageRequest);
                    } else {
                        showImageProgress(false);
                        mProfilePicView.setVisibility(View.VISIBLE);
                        mProfilePicView.setImageResource(R.drawable.userpicture);
                        mProfilePictureTitle.setVisibility(View.VISIBLE);
                        mProfilePictureTitle.setText("No Image On the Server. Tap To Upload One");
                    }



                    mFirstNameView.setText( response.getString("first_name") );  //NULL AITASE
                    mLastNameView.setText(response.getString("last_name"));
                    mEmailView.setText(response.getString("email"));
                    mPasswordView.setText("");
                    mPasswordView.setTypeface(Typeface.DEFAULT);
                    mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
                    mNidView.setText(response.getString("nid"));
                    mPhoneNumberView.setText( response.getString("mobile") );
                    mAddressView.setText( response.getString("address") );
                    mDateOfBirthView.setText( response.getString("dob") );

//                    if (response.getString("first_name"). ){
//                        mFirstNameView.setText( response.getString("first_name") );
//                    }
//                    if (response.getString("last_name").equals(null)){
//                        mLastNameView.setText(response.getString("last_name"));
//                    }
//                    if (response.getString("email").equals(null)){
//                        mEmailView.setText(response.getString("email"));
//                    }
//                    mPasswordView.setText("");
//                    mPasswordView.setTypeface(Typeface.DEFAULT);
//                    mPasswordView.setTransformationMethod(new PasswordTransformationMethod());
//                    if (response.getString("nid").equals(null)){
//                        mNidView.setText(response.getString("nid"));
//                    }
//                    if (response.getString("mobile").equals(null)){
//                        mPhoneNumberView.setText( response.getString("mobile") );
//                    }
//                    if (response.getString("address").equals(null)){
//                        mAddressView.setText( response.getString("address") );
//                    }
//                    if (response.getString("dob").equals(null)){
//                        mDateOfBirthView.setText( response.getString("dob") );
//                    }

                    //showProgress(false);
                } catch (JSONException e) {
                    e.printStackTrace();
                    showProgress(false);
                    Toast.makeText(view.getContext(), "SERVER BUSY.RESPONSE ERROR."+e.toString(), Toast.LENGTH_LONG).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                showProgress(false);
                Toast.makeText(view.getContext(), "SERVER OFFLINE. REQUEST FAILED."+error, Toast.LENGTH_LONG).show();
            }
        });
        queue.add(postRequest);

        mProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                String profileImage = null;
                try {
                    profileImage = getStringImage(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Map<String, String> updateProfileData = new HashMap<String, String>();
                updateProfileData.put("profileImage", profileImage);
                updateProfileData.put("profileImageType", fileType);
                updateProfileData.put("firstname", mFirstNameView.getText().toString().trim());
                updateProfileData.put("lastname", mLastNameView.getText().toString().trim());
                updateProfileData.put("email", mEmailView.getText().toString().trim());
                updateProfileData.put("password", mPasswordView.getText().toString().trim());
                updateProfileData.put("nid",mNidView.getText().toString().trim());
                updateProfileData.put("phone", mPhoneNumberView.getText().toString().trim());
                updateProfileData.put("address", mAddressView.getText().toString().trim());
                updateProfileData.put("dob", mDateOfBirthView.getText().toString().trim());

                Log.d("ProfileUpdateParams", updateProfileData.toString());
                showVolleyProgress(true);

                final String updateUrl = getString(R.string.host_url)+"/"+getString(R.string.profile_update)+"/"+getCurrentId().toString();

                RequestQueue profilequeue = Volley.newRequestQueue(view.getContext());

                JsonObjectRequest postProfileRequest = new JsonObjectRequest(Request.Method.POST, updateUrl, new JSONObject(updateProfileData), new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        showVolleyProgress(false);
                        try {
                            if (response.getBoolean("status")){
//                                Snackbar.make(mProgressView, "Profile Updated Successfully", Snackbar.LENGTH_LONG).show();
                                Snackbar.make(mProgressView, response.getString("message"), Snackbar.LENGTH_LONG).show();
                                mProfileButton.setBackgroundResource(R.drawable.home_button_green);
                                /*REFRESHING THE ACTIVITY WHEN UPDATE SUCCESFULLY*/


//                                Toast.makeText(view.getContext(), "Profile Updated Successfully!", Toast.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(mProgressView, "Profile Updated Failed. Try again!", Snackbar.LENGTH_LONG).show();
                                mProfileButton.setBackgroundResource(R.drawable.home_button_red);
//                                Toast.makeText(view.getContext(), "Update Failed"+response.getBoolean("message"), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Snackbar.make(mProgressView, "RESPONSE ERROR."+e.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
                            mProfileButton.setBackgroundResource(R.drawable.home_button_red);
//                            Toast.makeText(view.getContext(), "JSON Exception"+e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        showVolleyProgress(false);
                        Snackbar.make(mProgressView, "REQUEST FAILED."+error.getMessage(), Snackbar.LENGTH_INDEFINITE).show();
                        mProfileButton.setBackgroundResource(R.drawable.home_button_red);
//                        Toast.makeText(view.getContext(), "Response Error Listener"+error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
//                EDITING THE DEFAULT RETRY POLICIES.
                postProfileRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS*2, 1, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                profilequeue.add(postProfileRequest);
            }
        });
    }

    private String getCurrentId(){
        return pref.getString("logginID", "");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {

    }
}

